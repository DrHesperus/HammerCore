package com.zeitheron.hammercore.client.model.simple;

/**
 * Some opcodes for simple models.
 */
public interface ModelOpcodes
{
	public static final byte DFACE = 0x00;
	public static final byte EFACE = 0x01;
	public static final byte COLOR = 0x02;
	public static final byte BOUNDS = 0x03;
	public static final byte DRAW = 0x04;
	public static final byte DFACES = 0x05;
	public static final byte EFACES = 0x06;
	public static final byte ITEX = 0x07;
	public static final byte INAME = 0x08;
}