package com.zeitheron.hammercore.utils;

public interface IRegisterListener
{
	void onRegistered();
}