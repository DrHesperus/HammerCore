package com.zeitheron.hammercore.utils;

public interface OffthreadRunnable extends Runnable
{
	boolean isDone();
}